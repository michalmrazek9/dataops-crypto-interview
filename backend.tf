provider "google" {
  project                     = local.project_id
  impersonate_service_account = "sa-terraform@cosmic-tensor-369215.iam.gserviceaccount.com"
}

provider "google-beta" {
  project                     = local.project_id
  impersonate_service_account = "sa-terraform@cosmic-tensor-369215.iam.gserviceaccount.com"
}

terraform {
  backend "gcs" {
    bucket                      = "cosmic-tensor-369215-state"
    prefix                      = "terraform-state"
    impersonate_service_account = "sa-terraform@cosmic-tensor-369215.iam.gserviceaccount.com"
  }
}
