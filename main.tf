resource "google_storage_bucket" "staging_bucket" {
  name                        = "${local.project_id}-staging-bucket"
  location                    = local.region
  project                     = local.project_id
  uniform_bucket_level_access = true
  versioning {
    enabled = true
  }
}

resource "google_pubsub_subscription" "ethereum_transactions" {
  project = local.project_id
  name    = "ethereum_transactions"
  topic   = "projects/crypto-public-data/topics/crypto_ethereum.transactions"
}

resource "google_bigquery_dataset" "tatum_crypto" {
  dataset_id = "tatum_crypto"
  location   = "EU"
}


resource "google_bigquery_table" "ethereum_gas" {
  dataset_id = google_bigquery_dataset.tatum_crypto.dataset_id
  table_id   = "ethereum_gas"
  schema     = <<EOF
[
  {
    "name": "avg_gas_price",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "total_gas",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "num_transactions",
    "type": "INTEGER",
    "mode": "NULLABLE"
  },
  {
    "name": "window_start",
    "type": "TIMESTAMP",
    "mode": "NULLABLE"
  },
  {
    "name": "window_end",
    "type": "TIMESTAMP",
    "mode": "NULLABLE"
  }
]
EOF
  time_partitioning {
    type  = "DAY"
    field = "window_start"
  }
}

resource "google_dataflow_flex_template_job" "ethereum_gas" {
  provider                = google-beta
  name                    = "ethereum-gas"
  region                  = local.region
  container_spec_gcs_path = "gs://cosmic-tensor-369215-staging-bucket/tatum/crypto/templates/ethereum-gas.json"
  on_delete               = "cancel"
  parameters = {
    input_subscription = google_pubsub_subscription.ethereum_transactions.id
    output_table       = google_bigquery_table.ethereum_gas.id
    window_seconds     = 60
  }
}