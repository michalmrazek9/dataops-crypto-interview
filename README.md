# Dataops Crypto Interview

## The goal
The goal of this mini-project was to demonstrate the ability to use cloud tools and automation to deliver insights.
I decided to set a pipeline for processing Ethereum transactions and analyzing gas over time.

## Repository
This repository contains Terraform code for the whole pipeline and also code for Dataflow Flex Template.

## Data used
I am using Bitcoin transactions data that are publicly available in Pub/Sub topic
`projects/crypto-public-data/topics/crypto_ethereum.transactions`.
More detailed information can be found at https://github.com/blockchain-etl/public-datasets/blob/master/pubsub.md

## Data processing pipeline
I have created a Pub/Sub subscription to the public ETH transactions topic. Data are read with Dataflow pipeline
and aggregated in fixed time window - this was done with Dataflow Flex Template and Python Apache Beam SDK.
From Dataflow pipeline it writes data to BigQuery table and finally visualize the results with Looker.

![GCP architecture](images/gcp_architecture.png)

## Dataflow job
Dataflow job reads the data from Pub/Sub subscription and aggregates it in fixed windows (eg one window for one minute)
and then computes some statistics such as average fee, total fee and number of transactions. For computing statistic, I decided 
to use SqlTransform Apache Beam transformation.

![Dataflow pipeline](images/dataflow_pipeline.png)

## Results
I decided to plot number of ETH transactions and total used gas per minute. I also computed average gas per minute,
but that chart was too random, we would have to use longer time window.

For plotting I decided to use Looker because of its simple integration with BigQuery. In the dashboard we see
live data for the last 60 minutes.

![Looker dashboard](images/looker_dashboard.png)

We dont see any major trend from the one-hour data. It would be more interesting to compare in the longer time period
or use this dashboard for possible alerting.

## Building Dataflow Flex Template
First we need to build a docker container (I used GCP loud Build), push it to Artifact Registry
and then create the json template.

```
export PROJECT="$(gcloud config get-value project)"
export BUCKET=cosmic-tensor-369215-staging-bucket
export TEMPLATE_IMAGE="gcr.io/$PROJECT/tatum/crypto/ethereum-gas:latest"
export TEMPLATE_PATH="gs://$BUCKET/tatum/crypto/templates/ethereum-gas.json"

gcloud builds submit --tag "$TEMPLATE_IMAGE" .

gcloud dataflow flex-template build $TEMPLATE_PATH \
  --image "$TEMPLATE_IMAGE" \
  --sdk-language "PYTHON" \
  --metadata-file "metadata.json"
```

## My notes on todos and problems
One of my biggest problems was to actually choose interesting metric as I have never worked with blockchain data before.
For that reason, I decided to start with something simple as transactions gas. I have to say I learned a lot just by going 
through publicly available blockchain data and when I was trying to understand it.

Then I got stuck for some time on creating Dataflow job. Dataflow has a support for SQL job what would fulfill
my needs, but it was impossible to set with Terraform. So I decided to go with Python Apache Beam SDK. I started the job with 
python and Dataflow runner. But putting that to Terraform again was a bit tricky, because I had to create a Flex Template 
(basically a docker image in GCP Artifact registry) and set all the parameters in the correct way so it would work.

Also I was considering two solutions: Either do the windowing and aggregations in Dataflow and just stream the data to BigQuery
and do the computations directly there. Both had pros and cons (Dataflow solution is more heavy on computation and less on storage.
BigQuery would allow use to potentially use data in more ways). In the end I decided for Dataflow because it would more
suit for the task I had for this challenge. But in production setting, it might be more open to discussion.

And final challenge for me was to keep it short and not spend too much time on this task, because I can imagine
this taking several days if I wanted to have it really perfect :)

## My last note
Thank you for this challenge. I think this was the most enjoyable challenge I ever had for a job interview. Even if you dont decide to progress with my application, I feel that I have learned a lot about blockchain and crypto just going through the data. This gives me feeling that I would really enjoy that job. Anyway, thank you and enjoy.

