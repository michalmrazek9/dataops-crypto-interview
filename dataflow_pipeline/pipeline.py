#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import logging
import argparse
import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.transforms.sql import SqlTransform


def run(input_subscription, window_seconds, output_table, pipeline_args):
  pipeline_options = PipelineOptions(
      pipeline_args, save_main_session=True, streaming=True)

  with beam.Pipeline(options=pipeline_options) as pipeline:
    _ = (
        pipeline
        | "PubSub read" >>beam.io.ReadFromPubSub(
            subscription=input_subscription).with_output_types(bytes)
        | "Parse JSON payload" >> beam.Map(json.loads)
        # Use beam.Row to create a schema-aware PCollection
        | "Create beam Row" >> beam.Map(
            lambda x: beam.Row(
                receipt_gas_used=str(x['receipt_gas_used']),
                gas_price=int(x['gas_price'])))
        | "Fixed windows" >> beam.WindowInto(beam.window.FixedWindows(window_seconds))
        | "SQL Transform" >> SqlTransform(
            """
             SELECT
               CAST(AVG(gas_price) AS int) AS avg_gas_price,
               CAST(SUM(receipt_gas_used) AS int) AS total_gas,
               CAST(COUNT(*) AS int) AS num_transactions
             FROM PCOLLECTION
             """)
        | "Assemble Dictionary" >> beam.Map(
            lambda row,
            window=beam.DoFn.WindowParam: {
                "avg_gas_price": row.avg_gas_price,
                "total_gas": row.total_gas,
                "num_transactions": row.num_transactions,
                "window_start": window.start.to_rfc3339(),
                "window_end": window.end.to_rfc3339()
            })
        | "BigQuery write" >> beam.io.WriteToBigQuery(
    output_table,
    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND))


if __name__ == '__main__':
  logging.getLogger().setLevel(logging.INFO)

  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--input_subscription',
      dest='input_subscription',
      required=True,
      help=(
          'Cloud PubSub subscription to read from'))
  parser.add_argument(
      '--window_seconds',
      dest='window_seconds',
      required=True,
      type=int,
      help=(
          'Size of a window for computations in seconds'))
  parser.add_argument(
      '--output_table',
      dest='output_table',
      required=True,
      help=(
          'Cloud BigQuery table to write to'))
  known_args, pipeline_args = parser.parse_known_args()

  run(known_args.input_subscription, known_args.window_seconds, known_args.output_table, pipeline_args)